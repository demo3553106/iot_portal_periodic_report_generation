const AWS = require('aws-sdk');

const DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME = 'distance_aggregate_ddb';

const createDistanceAggregatedDb = (config) => {
    AWS.config.update(config);

    const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    
    const paramsCheck = {
        TableName: DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME
    };

    const paramsCreate = {
        AttributeDefinitions: [
            { AttributeName: 'device_id', AttributeType: 'N' },
            { AttributeName: 'timestamp', AttributeType: 'N' }
        ],
        KeySchema: [
            { AttributeName: 'device_id', KeyType: 'HASH' },
            { AttributeName: 'timestamp', KeyType: 'RANGE' }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 5, 
            WriteCapacityUnits: 5 
        },
        TableName: DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME
    };

    dynamodb.describeTable(paramsCheck, (err, data) => {
        if (err) {
            if (err.code === 'ResourceNotFoundException') {
                console.log(`Table '${DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME}' does not exist.`);
                
                //create table 
                dynamodb.createTable(paramsCreate, (err, data) => {
                    if (err) {
                        console.error('Error creating table:', err);
                    } else {
                        console.log('Aggregated table created successfully:', data);
                    }
                });
            } else {
                console.error('Error describing table:', err);
            }
        }
    });
}

// Function to create data record in the DynamoDB table
const storeDistanceAggregateData = (dataObject, config) => {
    AWS.config.update(config);

    const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-08-10' });
    
    const params = {
        TableName: DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME,
        Item: {
            'device_id': { N: dataObject.device_id.toString() },
            'timestamp': { N: dataObject.timestamp.toString() },
            'distance_travelled': { N: dataObject.distance_travelled.toString() },
            'time_frame': { S: dataObject.time_frame }
        }
    };

    dynamodb.putItem(params, (err, data) => {
        if (err) {
            console.error('Error creating data:', err);
        } else {
            console.log('Aggregated data created successfully:', dataObject);
        }
    });
}

module.exports = {
    createDistanceAggregatedDb: createDistanceAggregatedDb,
    storeDistanceAggregateData: storeDistanceAggregateData
}