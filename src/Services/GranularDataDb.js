const AWS = require('aws-sdk');

const GRANULAR_DATA_DDB_TABLE_NAME = "granular_data_ddb";

const fetchGranularDataDistance = (timestamp, deviceId, config) => {
    AWS.config.update(config);

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    // Define parameters for the query
    const params = {
        TableName: GRANULAR_DATA_DDB_TABLE_NAME,
        KeyConditionExpression: '#deviceId = :deviceId AND #timestamp < :timestamp',
        ExpressionAttributeNames: {
            '#deviceId': 'device_id',
            '#timestamp': 'timestamp'
        },
        ExpressionAttributeValues: {
            ':deviceId': deviceId,
            ':timestamp': timestamp
        },
        ScanIndexForward: false,
        Limit: 1
    };
    
    return new Promise((resolve, reject) =>{
        dynamodb.query(params, (err, data) => {
            if (err) {
                console.error('Unable to query latest granular data. Error:', JSON.stringify(err, null, 2));
                resolve(0);
            } else {
                console.log('Latest granular data query succeeded:', JSON.stringify(data, null, 2));
                resolve(JSON.parse(data.Items[0].data).distance_travelled);
            }
        });
    });
}


module.exports = {
    fetchGranularDataDistance: fetchGranularDataDistance
}