const cron = require('node-cron');
const dotEnvFlow = require('dotenv-flow');
const {fetchGranularDataDistance} = require('./Services/GranularDataDb');
const {createDistanceAggregatedDb, storeDistanceAggregateData} = require('./Services/DistanceAggregateDb')

// load .env file
dotEnvFlow.config({ node_env: process.env });

// load applicable / allowed environment variables
const {
	AWS_REGION,
	AWS_ACCESS_KEY,
	AWS_SECRET_KEY
} = process.env;

const AWS_CONFIG = { region: AWS_REGION, credentials:{accessKeyId : AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY } };

const generateReport = async () => {
    // Generate hourly report
    const deviceIdList = [1];
    deviceIdList.map( async (deviceId) => {
        const curentHourTime = (new Date).getTime();
        const oneHourBackTime = curentHourTime - 60*60*1000;
        const latestDistance = await fetchGranularDataDistance(curentHourTime, deviceId, AWS_CONFIG);
        const oneHourBackDistance = await fetchGranularDataDistance(oneHourBackTime, deviceId, AWS_CONFIG);
        const distance = latestDistance - oneHourBackDistance;
        console.log(`Last one hour, distance travelled by deviceId-${deviceId} : ` + distance);
        const aggregatedDataObj = { device_id: deviceId, timestamp: oneHourBackTime, distance_travelled: distance, time_frame: "hour"};
        if(distance) storeDistanceAggregateData(aggregatedDataObj, AWS_CONFIG);
    });

    if((new Date()).getHours() === 0){
        deviceIdList.map(async (deviceId) => {
            const curentTime = (new Date).getTime();
            const oneDayBackTime = curentTime - 24*60*60*1000;
            const latestDistance = await fetchGranularDataDistance(curentTime, deviceId, AWS_CONFIG);
            const oneDayBackDistance = await fetchGranularDataDistance(oneDayBackTime, deviceId, AWS_CONFIG);
            const distance = latestDistance - oneDayBackDistance;
            console.log(`Last one day, distance travelled by deviceId-${deviceId}: ` + distance);
            const aggregatedDataObj = { device_id: deviceId, timestamp: oneDayBackTime, distance_travelled: distance, time_frame: "day"};
            if(distance) storeDistanceAggregateData(aggregatedDataObj, AWS_CONFIG);
        });
    }
};

// Define the cron job to execute the task once every hour
const cronJob = cron.schedule('0 * * * *', generateReport, {
  scheduled: true,
  timezone: 'Asia/Kolkata' // IST timezone
});

createDistanceAggregatedDb(AWS_CONFIG);

cronJob.start();